CC = gcc
CFLAGS = -ggdb3 -Wall -Werror -Wformat -Wextra -lm -std=c11 -O0
CS50_SRC = libcs50/src/cs50.c
LIBCS50 = libcs50/src/cs50.h

hello: pset1/hello.c 
	$(CC) $(CFLAGS) pset1/hello.c -o build/hello

mario: pset1/mario.c $(LIBCS50)
	$(CC) $(CFLAGS) $(CS50_SRC) pset1/mario.c -o build/mario

cash: pset1/cash.c $(LIBCS50)
	$(CC) $(CFLAGS) $(CS50_SRC) pset1/cash.c -o build/cash

credit: pset1/credit.c $(LIBCS50)
	$(CC) $(CFLAGS) $(CS50_SRC) pset1/credit.c -o build/credit

pset1: mario cash credit

caesar: pset2/caesar.c $(LIBCS50)
	$(CC) $(CFLAGS) $(CS50_SRC) pset2/caesar.c -o build/caesar

vigenere: pset2/vigenere.c $(LIBCS50)
	$(CC) $(CFLAGS) $(CS50_SRC) pset2/vigenere.c -o build/vigenere

pset2: caesar vigenere

all: pset1 pset2

clean:
	rm -f build/{hello,mario,cash,credit}
