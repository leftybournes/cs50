#include <stdio.h>
#include <ctype.h>
#include <string.h>

#include "../libcs50/src/cs50.h"

bool is_string_alpha(string keyword);

int main(int argc, char *argv[]) {
    string keyword = (string) argv[1];

    if (argc < 2) {
        printf("No keyword provided. Usage: ./vigenere {keyword}\n");
        return EXIT_FAILURE;
    } else if (argc > 2) {
        printf("Too many arguments. Usage: ./vigenere {keyword}\n");
        return EXIT_FAILURE;
    } else if (!is_string_alpha(keyword)) {
        printf("Usage: ./vigenere {keyword}\n");
        return EXIT_FAILURE;
    }

    int keyword_len = strlen(keyword);
    string plaintext = get_string("plaintext: ");

    printf("ciphertext: ");

    int j = 0;
    for (int i = 0, pc = plaintext[i]; pc != '\0'; ++i, pc = plaintext[i]) {
        if (islower(pc)) {
            printf("%c", (((pc - 'a') + (tolower(keyword[j % keyword_len]) -
                                         'a')) % 26) + 'a');
            ++j;
        } else if (isupper(pc)) {
            printf("%c", (((pc - 'A') + (tolower(keyword[j % keyword_len]) -
                                                'a')) % 26) + 'A');
            ++j;
        } else printf("%c", pc);
    }

    putchar('\n');

    return EXIT_SUCCESS;
}

bool is_string_alpha(string s) {
    bool string_is_alpha = true;
    int i = 0;

    while (s[i] != '\0') {
        if (!isalpha(s[i])) {
            string_is_alpha = false;
            break;
        }

        ++i;
    }

    return string_is_alpha;
}
