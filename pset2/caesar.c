#include <stdio.h>

#include "../libcs50/src/cs50.h"

int main(int argc, char *argv[]) {
    int key = 8;

    if (argc < 2) printf("No argument provided, key defaulted to 8.\n");
    else if (argc > 2) {
        printf("Too many arguments provided. Usage: ./caesar {key}\n");
        return EXIT_FAILURE;
    } else key = *argv[1] - '0';

    printf("key: %d\n", key);
    
    string plaintext = get_string("plaintext: ");

    printf("ciphertext: ");

    for (int i = 0, pc = plaintext[i]; pc != '\0'; ++i, pc = plaintext[i]) {
        if (pc >= 'a' && pc <= 'z')
            printf("%c", ((pc - 'a' + key) % 26) + 'a');
        else if (pc >= 'A' && pc <= 'Z')
            printf("%c", ((pc - 'A' + key) % 26) + 'A');
        else printf("%c", pc);
    }
    
    printf("\n");
    
    return EXIT_SUCCESS;
}
