#include <stdio.h>
#include "../libcs50/src/cs50.h"

int main(void) {
  const int DIME = 10;
  const int PENNY = 1;
  const int QUARTER = 25;
  const int NICKEL = 5;

  int change;
  
  while ((change = (int) (get_float("Change owed: ") * 100)) <= 0);
  int total_coins = 0;

  total_coins += change / QUARTER;
  change %= QUARTER;
  
  total_coins += change / DIME;
  change %= DIME;

  total_coins += change / NICKEL;
  change %= NICKEL;

  total_coins += change / PENNY;
  change %= PENNY;

  printf("%i\n", total_coins);
  
  return 0;
}
