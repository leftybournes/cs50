#include <stdio.h>
#include "../libcs50/src/cs50.h"

int main(void) {
  int height = get_int("Height: ");

  for (int i = 0; i < height; i++) {

    for (int j = height; j >= 0; j--) {
      if (j > i) {
	printf(" ");
      } else {
	printf("#");
      }
    }

    // gap between pyramids
    printf("  ");
    
    // print right side of pyramid    
    for (int j = i; j >= 0; j--) {
      if (j <= i) {
	printf("#");
      } else {
	printf(" ");
      }
    }

    printf("\n");
  }
  
  return 0;
}
