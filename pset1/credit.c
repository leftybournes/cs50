#include <stdio.h>
#include <math.h>

#include "../libcs50/src/cs50.h"

int main(void) {
    long card_num = get_long("Number: ");
    int main_digits = 0;
    int other_digits = 0;
    long curr_num = card_num;
    int nDigits = 0;
  
    while (curr_num > 0) {
    
        if (nDigits % 2 != 0) {
      
            if ((curr_num % 10) * 2 > 9) {
                main_digits += ((curr_num % 10) * 2) / 10;
                main_digits += ((curr_num % 10) * 2) % 10;
            } else {
                main_digits += (curr_num % 10) * 2;
            }
      
        } else {
            other_digits += curr_num % 10;
        }

        curr_num /= 10;
        nDigits++;
    }

    if ((main_digits + other_digits) % 10 == 0) {
        long divisor = pow(10, nDigits) / 100;
        if (nDigits == 16) {
            if ((card_num / divisor) == 51 ||
                (card_num / divisor) == 52 ||
                (card_num / divisor) == 53 ||
                (card_num / divisor) == 54 ||
                (card_num / divisor) == 55) {
                printf("MASTERCARD\n");
            } else {
                divisor *= 10;
                if ((card_num / divisor) == 4) {
                    printf("VISA\n");
                } else {
                    printf("INVALID\n");
                }
            }
        } else if (nDigits == 15) {
            if ((card_num / divisor) == 34 ||
                (card_num / divisor) == 37) {
                printf("AMEX\n");
            }
        } else if (nDigits == 13) {
            divisor *= 10; 
            if ((card_num / divisor) == 4) {
                printf("VISA\n");
            }
        }
    } else {
        printf("INVALID\n");
    }
  
    return 0;
}
