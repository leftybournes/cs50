# This is CS50

The cs50 [header](https://github.com/cs50/libcs50/releases) are not included and must
be pulled in after cloning this repo.

After grabbing the cs50 files, follow the compilation instructions on the cs50 repo.
